// 详情-共享信息
export default {
  // 获取登录信息url
  userUrl: '/tag/online/user',
  // 存储sessionStorage变量名称
  permissions: 'permissions',
  list: [

    {
      'menuId': '1',
      'name': '系统管理',
      'url': '',
      'icon': 'job',
      'list': [
        {
          'menuId': '11',
          'name': '数据源定时任务',
          'icon': 'editor',
          'url': 'job/schedule'
        }
      ]
    }

  ],
}

