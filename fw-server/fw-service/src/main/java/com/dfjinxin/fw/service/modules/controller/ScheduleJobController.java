/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.dfjinxin.fw.service.modules.controller;

import com.dfjinxin.fw.service.commons.api.PageUtils;
import com.dfjinxin.fw.service.commons.api.Response;
import com.dfjinxin.fw.service.modules.entity.ScheduleJobEntity;
import com.dfjinxin.fw.service.modules.service.ScheduleJobService;
import com.dfjinxin.fw.service.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//import org.springframework.security.access.prepost.PreAuthorize;

/**
 * 定时任务
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/tag/job/schedule")
public class ScheduleJobController {
	@Autowired
	private ScheduleJobService scheduleJobService;

	/**
	 * 定时任务列表
	 */
	@RequestMapping("/list")
	public Response list(@RequestParam Map<String, Object> params){
		PageUtils page = scheduleJobService.queryPage(params);

		return Response.ok().put("page", page);
	}

	/**
	 * 定时任务信息
	 */
	@RequestMapping("/info/{jobId}")
	public Response info(@PathVariable("jobId") Long jobId){
		ScheduleJobEntity schedule = scheduleJobService.getById(jobId);

		return Response.ok().put("schedule", schedule);
	}

	/**
	 * 保存定时任务
	 */
	@RequestMapping("/save")
	public Response save(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);

		scheduleJobService.saveJob(scheduleJob);

		return Response.ok();
	}

	/**
	 * 修改定时任务
	 */
	@RequestMapping("/update")
	public Response update(@RequestBody ScheduleJobEntity scheduleJob){
		ValidatorUtils.validateEntity(scheduleJob);

		scheduleJobService.update(scheduleJob);

		return Response.ok();
	}

	/**
	 * 删除定时任务
	 */
	@RequestMapping("/delete")
	public Response delete(@RequestBody Long[] jobIds){
		scheduleJobService.deleteBatch(jobIds);

		return Response.ok();
	}

	/**
	 * 立即执行任务
	 */
	@RequestMapping("/run")
	public Response run(@RequestBody Long[] jobIds){
		scheduleJobService.run(jobIds);

		return Response.ok();
	}

	/**
	 * 暂停定时任务
	 */
	@RequestMapping("/pause")
	public Response pause(@RequestBody Long[] jobIds){
		scheduleJobService.pause(jobIds);

		return Response.ok();
	}

	/**
	 * 恢复定时任务
	 */
	@RequestMapping("/resume")
	public Response resume(@RequestBody Long[] jobIds){
		scheduleJobService.resume(jobIds);

		return Response.ok();
	}

}
