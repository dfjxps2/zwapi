package com.dfjinxin.fw.service.mpp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxTodayOutislandsInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 今日离岛旅客值机安检信息
 */
@Repository
@Mapper
public interface TsgptZhxTodayOutislandsInfoMpp extends BaseMapper<TsgptZhxTodayOutislandsInfo> {

}
