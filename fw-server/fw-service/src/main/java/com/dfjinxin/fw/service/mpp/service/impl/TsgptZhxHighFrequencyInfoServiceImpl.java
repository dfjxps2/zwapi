package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxHighFrequencyInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHighFrequencyInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHighFrequencyInfoService;
import org.springframework.stereotype.Service;

/**
 * 旅客低价高频信息
 */
@Service
public class TsgptZhxHighFrequencyInfoServiceImpl extends ServiceImpl<TsgptZhxHighFrequencyInfoMpp, TsgptZhxHighFrequencyInfo> implements TsgptZhxHighFrequencyInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxHighFrequencyInfo info) {
        return this.save(info);
    }

}
