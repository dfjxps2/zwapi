package com.dfjinxin.fw.service.aspect;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class BusiLog implements Serializable {

    private String ip;//请求IP

    private String requestPra;//请求参数

    private Object response;//返回结果

    private String methodName;//请求方法

    private String className;//请求类
}
