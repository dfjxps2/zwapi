package com.dfjinxin.fw.service.api.service.busi;

import com.dfjinxin.fw.service.api.service.FwApiService;
import com.dfjinxin.fw.service.api.service.RunService;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxAnomalousInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取民航离岛旅客安检异常信息接口 180天
 */
@Slf4j
@Service
public class Api31All180Service implements RunService {

    @Autowired
    private TsgptZhxAnomalousInfoService tsgptZhxAnomalousInfoService;

    @Autowired
    private Api314Service api314Service;

    @Autowired
    private Api316Service api316Service;

    @Autowired
    private Api318Service api318Service;

    @Autowired
    private FwApiService fwApiService;

    @Override
    public void run(String param) {

        try {
            log.info("【获取民航离岛旅客安检异常信息接口】执行参数：{}", param);

            List<PassengerDTO> passengerAbnDTOList = SmpPsrResource.queryAbnormalSecInfo(param);
            if(null != passengerAbnDTOList && passengerAbnDTOList.size() > 0){
                log.info("接收总数：{}", passengerAbnDTOList.size());
                for(PassengerDTO dto : passengerAbnDTOList){

                    /**
                     * 旅客安检信息
                     */
                    tsgptZhxAnomalousInfoService.createByDto(dto);

                    /**
                     * 获取历史半年民航旅客进出岛订票信息接口
                     */
                    api314Service.run(dto.getCertNo());

                    /**
                     * 获取历史半年民航离岛旅客值机安检信息接口
                     */
                    api316Service.run(dto.getCertNo());

                    /**
                     * 获取历史半年民航进出岛旅客同行人信息接口
                     */
                    api318Service.run(dto.getCertNo());
                }
            }
        }catch (Throwable e) {
            fwApiService.throwableExeception("获取民航离岛旅客安检异常信息接口 180天", e);
        }

    }
}
