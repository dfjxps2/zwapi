package com.dfjinxin.fw.service.mpp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearColleaguesInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 历史半年旅客进出岛同行人信息
 */
@Repository
@Mapper
public interface TsgptZhxHalfyearColleaguesInfoMpp extends BaseMapper<TsgptZhxHalfyearColleaguesInfo> {

}
