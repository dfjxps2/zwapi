package com.dfjinxin.fw.service.mpp.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 旅客安检信息
 */
@Data
@Accessors(chain = true)
@TableName("t_sgpt_zhx_anomalous_info")
public class TsgptZhxAnomalousInfo implements Serializable {

    public TsgptZhxAnomalousInfo(){
        super();
    }

    public TsgptZhxAnomalousInfo(PassengerDTO dto){
        super();
        this.pasname = dto.getPasName(); //旅客姓名
        this.certtype = dto.getCertType(); //旅客证件类型
        this.certno = dto.getCertNo(); //旅客证件号码
    }

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    private String pasname; //旅客姓名

    private String certtype; //旅客证件类型

    private String certno; //旅客证件号码

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createtm;

}
