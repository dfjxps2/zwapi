package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxTodayOutislandsInfo;

/**
 * 今日离岛旅客值机安检信息
 */
public interface TsgptZhxTodayOutislandsInfoService extends IService<TsgptZhxTodayOutislandsInfo> {

    boolean create(TsgptZhxTodayOutislandsInfo info);
}
