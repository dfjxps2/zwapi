//package com.dfjinxin.fw.service.api.service;
//
//import com.dfjinxin.fw.service.api.constant.FwConstant;
//import com.dfjinxin.fw.service.api.constant.RSAUtils;
//import com.dfjinxin.fw.service.modules.entity.RequestToken;
//import com.dfjinxin.fw.service.modules.service.RequestTokenService;
//import com.travelsky.dib.odats.profpsr.ms.bas.config.NativaMscBuild;
//import com.travelsky.dss.sc.nativeclient.msc.web.context.AbstractTokenStorage;
//import com.travelsky.dss.sc.nativeclient.msc.web.context.dto.TokenContentDTO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Slf4j
//@Service
//public class MyAbstractTokenStorage extends AbstractTokenStorage {
//
//    @Value("${api-server-url}")
//    private String authUrl;
//
//    @Autowired
//    private RequestTokenService requestTokenService;
//
//    @Override
//    public void putToken(String systemId, TokenContentDTO token) {
//        log.info("接收token：{}, {}", systemId, token);
//        RequestToken requestToken = new RequestToken();
//        requestToken.setSystemId(systemId);
//        requestToken.setToken(token.getToken());
//        requestToken.setStatus(token.getOfflineStatus());
//        requestTokenService.save(requestToken);
//    }
//
//    @Override
//    public TokenContentDTO getToken(String systemId) {
//        log.info("查询token:{}", systemId);
//        TokenContentDTO tokenContentDTO = new TokenContentDTO();
//        RequestToken requestToken = requestTokenService.queryToken(systemId);
//        tokenContentDTO.setToken(requestToken.getToken());
//        if(requestToken.getStatus()){
//            tokenContentDTO.setOfflineStatusToDown();
//        }
//        log.info("返回token:{}", tokenContentDTO);
//        return tokenContentDTO;
//    }
//
//    @Override
//    public void removeToken(String systemId) {
//
//    }
//
//}
