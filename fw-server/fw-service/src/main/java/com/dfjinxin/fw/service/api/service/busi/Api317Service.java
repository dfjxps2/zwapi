package com.dfjinxin.fw.service.api.service.busi;

import com.dfjinxin.fw.service.api.service.FwApiService;
import com.dfjinxin.fw.service.api.service.RunService;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeColleaguesInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxThreeColleaguesInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.CompanionDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取今明后民航进出岛旅客同行人信息接口
 */
@Slf4j
@Service
public class Api317Service implements RunService {

    @Autowired
    private TsgptZhxThreeColleaguesInfoService tsgptZhxThreeColleaguesInfoService;
    @Autowired
    private IdGenerator idGenerator;
    @Autowired
    private FwApiService fwApiService;

    @Override
    public void run(String param) {

        /**
         *  pasName String 张三 同行人旅客姓名
         *  certType String NI 同行人旅客证件类型
         *  certNo String 12345678 同行人旅客证件号
         *  fltDate String 2020-07-02 航班日期
         *  airln String 海南航空 航空公司
         *  dptAirport String 海口美兰 起始机场
         *  dptCity String 海口 起始城市
         *  arrAirport String 北京首都 到达机场
         *  arrCity String 北京 到达城市
         */

        try {
            List<CompanionDTO> companionDTOList = SmpPsrResource.queryCompanionByCert(param);
            if(null != companionDTOList && companionDTOList.size() > 0){
                log.info("旅客证件号：{}, 接收总数：{}", param, companionDTOList.size());
                for(CompanionDTO dto : companionDTOList){

                    /**
                     * 今明后旅客进出岛同行人信息
                     */
                    TsgptZhxThreeColleaguesInfo info = new TsgptZhxThreeColleaguesInfo(param, dto);
                    info.setId("" + idGenerator.getId());
                    tsgptZhxThreeColleaguesInfoService.create(info);
                }
            }
        }catch (Throwable e) {
            fwApiService.throwableExeception("获取今明后民航进出岛旅客同行人信息接口", e);
        }

    }
}
