/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.dfjinxin.fw.service.modules.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.modules.entity.ScheduleJobEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * 定时任务
 *
 * @author Mark sunlightcs@gmail.com
 */
@Repository
@Mapper
public interface ScheduleJobDao extends BaseMapper<ScheduleJobEntity> {
	
	/**
	 * 批量更新状态
	 */
	@Update("<script>" +
				"update schedule_job set status = #{status} where job_id in\n" +
				"\t\t<foreach item=\"jobId\" collection=\"list\"  open=\"(\" separator=\",\" close=\")\">\n" +
				"\t\t\t#{jobId}\n" +
				"\t\t</foreach>" +
			"</script>")
	int updateBatch(Map<String, Object> map);
}
