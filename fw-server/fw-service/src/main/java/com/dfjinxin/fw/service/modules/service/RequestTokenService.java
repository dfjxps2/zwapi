package com.dfjinxin.fw.service.modules.service;

import com.dfjinxin.fw.service.modules.entity.RequestToken;

public interface RequestTokenService {

    RequestToken queryToken(String systemId);

    Boolean save(RequestToken requestToken);

}
