package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeColleaguesInfo;

/**
 * 今明后旅客进出岛同行人信息
 */
public interface TsgptZhxThreeColleaguesInfoService extends IService<TsgptZhxThreeColleaguesInfo> {

    boolean create(TsgptZhxThreeColleaguesInfo info);
}
