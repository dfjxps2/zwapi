package com.dfjinxin.fw.service.modules.controller;

import com.dfjinxin.fw.service.api.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private LoginService loginService;

    @GetMapping("/fw/test")
    public String test(){
        loginService.createToken();
        return "完成";
    }
}
