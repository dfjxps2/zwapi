package com.dfjinxin.fw.service.mpp.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.CkiTravelDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 历史半年离岛旅客值机安检信息
 */
@Data
@Accessors(chain = true)
@TableName("t_sgpt_zhx_halfyear_outislands_info")
public class TsgptZhxHalfyearOutislandsInfo implements Serializable {

    public TsgptZhxHalfyearOutislandsInfo(){
        super();
    }

    public TsgptZhxHalfyearOutislandsInfo(CkiTravelDTO dto){
        super();
        /**
         *''旅客姓名'
         */
        this.pasname = dto.getPasName();

        /**
         *''旅客证件类型'
         */
        this.certtype = dto.getCertType();

        /**
         *''旅客证件号码'
         */
        this.certno = dto.getCertNo();

        /**
         *''航班号'
         */
        this.fltnumber = dto.getFltNumber();

        /**
         *''航班日期'
         */
        this.fltdate = dto.getFltDate();
        /**
         *''航空公司'
         */
        this.airln = dto.getAirln();

        /**
         *''起始机场'
         */
        this.dptairport = dto.getDptAirport();

        /**
         *''起始城市'
         */
        this.dptcity = dto.getDptCity();

        /**
         *''到达机场'
         */
        this.arrairport = dto.getArrAirport();

        /**
         *''到达城市'
         */
        this.arrcity = dto.getArrCity();

        /**
         *''航班状态（ y 正常 n 取消）'
         */
        this.fltstatus = dto.getFltStatus();

        /**
         *''是否托运'
         */
        this.hasbag = dto.getHasBag();

        /**
         *''托运行李件数'
         */
        this.bagcount = dto.getBagCount();

        /**
         *''托运行李重量（ kg）'
         */
        this.bagweight = dto.getBagWeight();

        /**
         *''安检通道'
         */
        this.secname = dto.getSecName();
    }

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     *''旅客姓名'
     */
    private String pasname;

    /**
     *''旅客证件类型'
     */
    private String certtype;

    /**
     *''旅客证件号码'
     */
    private String certno;

    /**
     *''航班号'
     */
    private String fltnumber;

    /**
     *''航班日期'
     */
    private String fltdate;
    /**
     *''航空公司'
     */
    private String airln;

    /**
     *''起始机场'
     */
    private String dptairport;

    /**
     *''起始城市'
     */
    private String dptcity;

    /**
     *''到达机场'
     */
    private String arrairport;

    /**
     *''到达城市'
     */
    private String arrcity;

    /**
     *''航班状态（ y 正常 n 取消）'
     */
    private String fltstatus;

    /**
     *''是否托运'
     */
    private String hasbag;

    /**
     *''托运行李件数'
     */
    private Integer bagcount;

    /**
     *''托运行李重量（ kg）'
     */
    private Integer bagweight;

    /**
     *''安检通道'
     */
    private String secname;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createtm;
}
