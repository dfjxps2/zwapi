package com.dfjinxin.fw.service.api.service.busi;

import com.alibaba.fastjson.JSONObject;
import com.dfjinxin.fw.service.api.constant.FwConstant;
import com.dfjinxin.fw.service.api.service.FwApiService;
import com.dfjinxin.fw.service.api.service.RunService;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHighFrequencyInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHighFrequencyInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import com.travelsky.dss.sc.nativeclient.msc.context.TokenStorageContext;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthException;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthTokenOfflineException;
import com.travelsky.dss.sc.nativeclient.msc.exception.RequestBusinessRuntimeException;
import com.travelsky.dss.sc.nativeclient.msc.exception.dto.BusinessExceptionInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取民航进出岛旅客低价高频信息接口
 */
@Slf4j
@Service
public class Api312Service implements RunService {

    @Autowired
    private TsgptZhxHighFrequencyInfoService tsgptZhxHighFrequencyInfoService;
    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private FwApiService fwApiService;

    @Override
    public void run(String param) {
        JSONObject jsonObject = JSONObject.parseObject(param);

        /**
         * pasName String 张三 旅客姓名
         * certType String NI 旅客证件类型
         * certNo String 12345678 旅客证件号
         */
        try {
            int travelCount = jsonObject.getInteger("travelCount");
            int priceDiscount = jsonObject.getInteger("priceDiscount");
            List<PassengerDTO> passengerDTOActList = SmpPsrResource.queryPasActivityInfo(travelCount ,priceDiscount);

            if(null != passengerDTOActList && passengerDTOActList.size() > 0){
                log.info("travelCount: {}, priceDiscount: {}, 接收总量：{}", travelCount, priceDiscount, passengerDTOActList.size());
                for(PassengerDTO dto : passengerDTOActList){

                    /**
                     * 旅客低价高频信息
                     */
                    TsgptZhxHighFrequencyInfo info = new TsgptZhxHighFrequencyInfo(dto);
                    info.setId("" + idGenerator.getId());

                    tsgptZhxHighFrequencyInfoService.create(info);
                }
            }
        }catch (Throwable e) {
            fwApiService.throwableExeception("获取民航进出岛旅客低价高频信息接口", e);
        }

    }
}
