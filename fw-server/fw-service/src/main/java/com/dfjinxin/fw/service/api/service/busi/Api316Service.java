package com.dfjinxin.fw.service.api.service.busi;

import com.alibaba.fastjson.JSONObject;
import com.dfjinxin.fw.service.api.service.FwApiService;
import com.dfjinxin.fw.service.api.service.RunService;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearOutislandsInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHalfyearOutislandsInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.CkiTravelDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取历史半年民航离岛旅客值机安检信息接口
 */
@Slf4j
@Service
public class Api316Service implements RunService {

    @Autowired
    private TsgptZhxHalfyearOutislandsInfoService tsgptZhxHalfyearOutislandsInfoService;
    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private FwApiService fwApiService;

    @Override
    public void run(String param) {

        /**
         *  pasName String 张三 旅客姓名
         *  certType String NI 旅客证件类型
         *  certNo String 12345678 旅客证件号
         *  fltNumber String HU7281 航班号
         *  fltDate String 2020-07-02 航班日期
         *  airln String 海南航空 航空公司
         *  dptAirport String 海口美兰 起始机场
         *  dptCity String 海口 起始城市
         *  arrAirport String 北京首都 到达机场 a
         *  rrCity String 北京 到达城市
         *  fltStatus String Y 航班状态（Y:正常 N:取 消）
         *  hasBag String Y 是否托运
         *  bagCount Integer 1 托运行李件数
         *  bagWeight Integer 23 托运行李重量（KG）
         *  secName String 国内安检 1 号 安检通道
         */

        try {
            List<CkiTravelDTO> ckiTravelHisDTOList = SmpPsrResource.queryCkiTravelHisByCert(param);
            if(null != ckiTravelHisDTOList && ckiTravelHisDTOList.size() > 0){
                log.info("旅客证件号：{}, 接收总数：{}", param, ckiTravelHisDTOList.size());
                for(CkiTravelDTO dto : ckiTravelHisDTOList){

                    /**
                     * 历史半年离岛旅客值机安检信息
                     */
                    TsgptZhxHalfyearOutislandsInfo info = new TsgptZhxHalfyearOutislandsInfo(dto);
                    info.setId("" + idGenerator.getId());
                    tsgptZhxHalfyearOutislandsInfoService.create(info);
                }
            }
        }catch (Throwable e) {
            fwApiService.throwableExeception("获取历史半年民航离岛旅客值机安检信息接口", e);
        }

    }
}
