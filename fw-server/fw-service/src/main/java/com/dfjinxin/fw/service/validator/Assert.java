/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.dfjinxin.fw.service.validator;

import com.dfjinxin.fw.service.commons.core.exception.ServiceException;
import com.dfjinxin.fw.service.commons.utils.CodeUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 数据校验
 *
 * @author Mark sunlightcs@gmail.com
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new ServiceException(CodeUtils.EXCEPTION_CODE, message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new ServiceException(CodeUtils.EXCEPTION_CODE, message);
        }
    }
}
