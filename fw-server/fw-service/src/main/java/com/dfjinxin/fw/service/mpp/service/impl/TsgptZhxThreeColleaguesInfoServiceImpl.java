package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxThreeColleaguesInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeColleaguesInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxThreeColleaguesInfoService;
import org.springframework.stereotype.Service;

/**
 * 今明后旅客进出岛同行人信息
 */
@Service
public class TsgptZhxThreeColleaguesInfoServiceImpl extends ServiceImpl<TsgptZhxThreeColleaguesInfoMpp, TsgptZhxThreeColleaguesInfo> implements TsgptZhxThreeColleaguesInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxThreeColleaguesInfo info) {
        return this.save(info);
    }

}
