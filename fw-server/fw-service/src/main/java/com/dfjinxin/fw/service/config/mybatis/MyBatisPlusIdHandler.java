package com.dfjinxin.fw.service.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import org.apache.ibatis.reflection.MetaObject;
import java.time.LocalDateTime;

public class MyBatisPlusIdHandler implements MetaObjectHandler {
    private static final String ID = "id";

    //创建时间
    private static final String CREATED_TM = "createtm";

    private final IdGenerator idGenerator;

    public MyBatisPlusIdHandler(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    /**
     * 判断id为空时自动填充IdGenerator生成器生成的id
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        if (metaObject == null) {
            return;
        }

        if (metaObject.hasGetter(ID) && metaObject.hasSetter(ID)
                && metaObject.getGetterType(ID).equals(Long.class)
                && metaObject.getSetterType(ID).equals(Long.class)) {
            Long value = (Long) metaObject.getValue(ID);
            if (value == null || value < 0) {
                metaObject.setValue(ID, idGenerator.getId());
            }
        }

        setFieldValByNameIfNull(CREATED_TM, LocalDateTime.now(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }

    private void setFieldValByNameIfNull(String fieldname, Object value, MetaObject metaObject) {
        if (metaObject.hasGetter(fieldname) && metaObject.hasSetter(fieldname)) {
            Object existValue = metaObject.getValue(fieldname);
            if (existValue == null) {
                setFieldValByName(fieldname, value, metaObject);
            }
        }
    }

    private void setFieldValByNameUpdate(String fieldname, Object value, MetaObject metaObject) {
        if (metaObject.hasGetter(fieldname) && metaObject.hasSetter(fieldname)) {
            setFieldValByName(fieldname, value, metaObject);
        }
    }
}
