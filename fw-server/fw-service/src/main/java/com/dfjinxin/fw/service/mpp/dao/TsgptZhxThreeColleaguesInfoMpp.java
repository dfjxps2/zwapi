package com.dfjinxin.fw.service.mpp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeColleaguesInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 今明后旅客进出岛同行人信息
 */
@Repository
@Mapper
public interface TsgptZhxThreeColleaguesInfoMpp extends BaseMapper<TsgptZhxThreeColleaguesInfo> {

}
