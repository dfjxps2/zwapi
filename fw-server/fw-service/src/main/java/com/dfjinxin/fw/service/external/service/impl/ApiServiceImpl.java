package com.dfjinxin.fw.service.external.service.impl;

import com.dfjinxin.fw.service.external.service.ApiService;
import com.dfjinxin.fw.service.mpp.entity.*;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.CkiTravelDTO;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.CompanionDTO;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.EtTravelDTO;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ApiServiceImpl implements ApiService {

    /**
     * 获取民航离岛旅客安检异常信息接口 180天
     */
    @Override
    public List<TsgptZhxAnomalousInfo> zhxApi31AllOrOneDay(String day){
        List<TsgptZhxAnomalousInfo> tsgptZhxAnomalousInfos = new ArrayList<>();

        List<PassengerDTO> passengerAbnDTOList = SmpPsrResource.queryAbnormalSecInfo(day);
        if(null != passengerAbnDTOList && passengerAbnDTOList.size() > 0) {
            log.info("接收总数：{}", passengerAbnDTOList.size());
            for (PassengerDTO dto : passengerAbnDTOList) {
                TsgptZhxAnomalousInfo info = new TsgptZhxAnomalousInfo(dto);
                tsgptZhxAnomalousInfos.add(info);
            }
        }

        return tsgptZhxAnomalousInfos;
    }


    /**
     * 获取民航进出岛旅客低价高频信息接口
     */
    @Override
    public List<TsgptZhxHighFrequencyInfo> zhxApi312(int travelCount, int priceDiscount){
        List<TsgptZhxHighFrequencyInfo> tsgptZhxHighFrequencyInfos = new ArrayList<>();

        List<PassengerDTO> passengerDTOActList = SmpPsrResource.queryPasActivityInfo(travelCount ,priceDiscount);

        if(null != passengerDTOActList && passengerDTOActList.size() > 0){
            log.info("travelCount: {}, priceDiscount: {}, 接收总量：{}", travelCount, priceDiscount, passengerDTOActList.size());
            for(PassengerDTO dto : passengerDTOActList){
                TsgptZhxHighFrequencyInfo info = new TsgptZhxHighFrequencyInfo(dto);
                tsgptZhxHighFrequencyInfos.add(info);
            }
        }
        return tsgptZhxHighFrequencyInfos;
    }


    /**
     * 获取今明后三天民航旅客进出岛订票信息接口
     */
    @Override
    public List<TsgptZhxThreeBookingInfo> zhxApi313(String param){
        List<TsgptZhxThreeBookingInfo> tsgptZhxThreeBookingInfos = new ArrayList<>();

        List<EtTravelDTO> etTravelDTOList = SmpPsrResource.queryEtTravelByCert(param);
        if(null != etTravelDTOList && etTravelDTOList.size() > 0){
            log.info("旅客证件号：{}, 接收总数：{}", param, etTravelDTOList.size());
            for(EtTravelDTO dto : etTravelDTOList){
                TsgptZhxThreeBookingInfo info = new TsgptZhxThreeBookingInfo(dto);
                tsgptZhxThreeBookingInfos.add(info);
            }
        }
        return tsgptZhxThreeBookingInfos;
    }


    /**
     * 获取历史半年民航旅客进出岛订票信息接口
     */
    @Override
    public List<TsgptZhxHalfyearBookingInfo> zhxApi314(String param){
        List<TsgptZhxHalfyearBookingInfo> tsgptZhxHalfyearBookingInfos = new ArrayList<>();

        List<EtTravelDTO> etTravelCkiDTOList = SmpPsrResource.queryEtTravelHisByCert(param);
        if(null != etTravelCkiDTOList && etTravelCkiDTOList.size() > 0){
            log.info("旅客证件号：{}, 接收总数：{}", param, etTravelCkiDTOList.size());
            for(EtTravelDTO dto : etTravelCkiDTOList){
                TsgptZhxHalfyearBookingInfo info = new TsgptZhxHalfyearBookingInfo(dto);
                tsgptZhxHalfyearBookingInfos.add(info);
            }
        }

        return tsgptZhxHalfyearBookingInfos;
    }


    /**
     * 获取当日民航离岛旅客值机安检信息接口
     */
    @Override
    public List<TsgptZhxTodayOutislandsInfo> zhxApi315(String param){
        List<TsgptZhxTodayOutislandsInfo> tsgptZhxTodayOutislandsInfos = new ArrayList<>();

        List<CkiTravelDTO> ckiTravelDTOList = SmpPsrResource.queryCkiTravelByCert(param);
        if(null != ckiTravelDTOList && ckiTravelDTOList.size() > 0){
            log.info("旅客证件号：{}, 接收总数：{}", param, ckiTravelDTOList.size());
            for(CkiTravelDTO dto : ckiTravelDTOList){
                TsgptZhxTodayOutislandsInfo info = new TsgptZhxTodayOutislandsInfo(dto);
                tsgptZhxTodayOutislandsInfos.add(info);
            }
        }

        return tsgptZhxTodayOutislandsInfos;
    }

    /**
     * 获取历史半年民航离岛旅客值机安检信息接口
     */
    @Override
    public List<TsgptZhxHalfyearOutislandsInfo> zhxApi316(String param){
        List<TsgptZhxHalfyearOutislandsInfo> tsgptZhxHalfyearOutislandsInfos = new ArrayList<>();

        List<CkiTravelDTO> ckiTravelHisDTOList = SmpPsrResource.queryCkiTravelHisByCert(param);
        if(null != ckiTravelHisDTOList && ckiTravelHisDTOList.size() > 0){
            log.info("旅客证件号：{}, 接收总数：{}", param, ckiTravelHisDTOList.size());
            for(CkiTravelDTO dto : ckiTravelHisDTOList){
                TsgptZhxHalfyearOutislandsInfo info = new TsgptZhxHalfyearOutislandsInfo(dto);
                tsgptZhxHalfyearOutislandsInfos.add(info);
            }
        }

        return tsgptZhxHalfyearOutislandsInfos;
    }

    /**
     * 获取今明后民航进出岛旅客同行人信息接口
     */
    @Override
    public List<TsgptZhxThreeColleaguesInfo> zhxApi317(String param){
        List<TsgptZhxThreeColleaguesInfo> tsgptZhxThreeColleaguesInfos = new ArrayList<>();

        List<CompanionDTO> companionDTOList = SmpPsrResource.queryCompanionByCert(param);
        if(null != companionDTOList && companionDTOList.size() > 0){
            log.info("旅客证件号：{}, 接收总数：{}", param, companionDTOList.size());
            for(CompanionDTO dto : companionDTOList){
                TsgptZhxThreeColleaguesInfo info = new TsgptZhxThreeColleaguesInfo(param, dto);
                tsgptZhxThreeColleaguesInfos.add(info);
            }
        }

        return tsgptZhxThreeColleaguesInfos;
    }

    /**
     * 获取历史半年民航进出岛旅客同行人信息接口
     */
    @Override
    public List<TsgptZhxHalfyearColleaguesInfo> zhxApi318(String param){
        List<TsgptZhxHalfyearColleaguesInfo> tsgptZhxHalfyearColleaguesInfos = new ArrayList<>();

        List<CompanionDTO> companionHisDTOList = SmpPsrResource.queryCompanionHisByCert(param);
        if(null != companionHisDTOList && companionHisDTOList.size() > 0){
            log.info("旅客证件号：{}, 接收总数：{}", param, companionHisDTOList.size());
            for(CompanionDTO dto : companionHisDTOList){
                TsgptZhxHalfyearColleaguesInfo info = new TsgptZhxHalfyearColleaguesInfo(param, dto);
                tsgptZhxHalfyearColleaguesInfos.add(info);
            }
        }

        return tsgptZhxHalfyearColleaguesInfos;
    }

}
