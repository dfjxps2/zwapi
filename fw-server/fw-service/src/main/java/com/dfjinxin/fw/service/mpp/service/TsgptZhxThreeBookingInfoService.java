package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeBookingInfo;

/**
 * 今明后三天进出岛旅客订票信息
 */
public interface TsgptZhxThreeBookingInfoService extends IService<TsgptZhxThreeBookingInfo> {

    boolean create(TsgptZhxThreeBookingInfo info);
}
