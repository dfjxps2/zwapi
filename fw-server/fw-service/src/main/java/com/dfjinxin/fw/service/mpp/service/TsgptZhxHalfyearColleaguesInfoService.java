package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearColleaguesInfo;

/**
 * 历史半年旅客进出岛同行人信息
 */
public interface TsgptZhxHalfyearColleaguesInfoService extends IService<TsgptZhxHalfyearColleaguesInfo> {

    boolean create(TsgptZhxHalfyearColleaguesInfo info);
}
