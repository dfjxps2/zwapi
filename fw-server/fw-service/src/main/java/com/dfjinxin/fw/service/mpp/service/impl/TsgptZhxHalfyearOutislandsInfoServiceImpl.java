package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxHalfyearOutislandsInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearOutislandsInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHalfyearOutislandsInfoService;
import org.springframework.stereotype.Service;

/**
 * 历史半年离岛旅客值机安检信息
 */
@Service
public class TsgptZhxHalfyearOutislandsInfoServiceImpl extends ServiceImpl<TsgptZhxHalfyearOutislandsInfoMpp, TsgptZhxHalfyearOutislandsInfo> implements TsgptZhxHalfyearOutislandsInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxHalfyearOutislandsInfo info) {
        return this.save(info);
    }

}
