package com.dfjinxin.fw.service.api.constant;

public enum UrlEnum {

    //oauth2登录
    LOGIN_URL("/api/auth/login"),
    clientuid("/api/verification/clientuid"),
    ;

    private String url;

    UrlEnum(String url) {
        this.url = url;

    }


    public String getUrl() {
        return url;
    }
}
