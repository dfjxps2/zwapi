package com.dfjinxin.fw.service.api.constant;

import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Java RSA 加密工具类
 */
@Slf4j
public class RSAUtils {

    /**
     * 密钥长度 于原文长度对应 以及越长速度越慢
     */
    private final static int KEY_SIZE = 1024;//117

    public static final String PUBLIC_KEY = "publicKey";
    public static final String PRIVATE_KEY = "privateKey";

    /**
     * 用于封装随机产生的公钥与私钥
     */
    public static Map<String, String> keyMap = new HashMap<>();

    static {
        try {
            keyMap.put(PUBLIC_KEY, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDCHTHPOZzpKm8zf3c8XBvZg1oQA5FPRd0JoxLx2SPbvEfDhe/izJ1YZdpe7X6cdNqAvI5TnySHi1JbWLXox/nr0OgKa3FVePPUh7VwqNW1zlSKdup3DMFcxG+gULSS0VPTyUxmZWkPkA+KJDrVjIKTQFrz62RFnTGmmrpKA3fa6wIDAQAB");
            keyMap.put(PRIVATE_KEY, "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMIdMc85nOkqbzN/dzxcG9mDWhADkU9F3QmjEvHZI9u8R8OF7+LMnVhl2l7tfpx02oC8jlOfJIeLUltYtejH+evQ6AprcVV489SHtXCo1bXOVIp26ncMwVzEb6BQtJLRU9PJTGZlaQ+QD4okOtWMgpNAWvPrZEWdMaaaukoDd9rrAgMBAAECgYEAliLAgLfd3hXdFmazUXMocy79/L7VqAuwL6D6qSY6bt0MYHT6ml+KH3H/2UY1lEtpKQMtzg2b0l5234IYiKgLavesPhgFZHEHkK25I0ERFNJEP3FTwtaecQoWbKcyMJL3uqEjywkzQwOlZlDwZKaFXNh7u40Pk5c3e768ipLBrckCQQD1PKU8Ccuh85IJGholUdcXLmvszzislfxbewTunu0i8MkGbpbebAmgXULxWhm80laOrjyhige6NQ9RI0BkELY3AkEAyqIoxA49aPFKtH7b28oZ+uzMdRc1IaoJlfZdph6OqSC8v40ek4brGp4h2rWYW51pkVLrai30N6qc7C+0aSUm7QJAXII+CoHfcP5CrnBMQJu4uaPnTnN0+5Wwlom3eJQ2/CE8k9KP0uOKVhYYNJc6ckIeKMCNWpqJA7MUThUwKw8qzQJAAbTr8vrLuZ6OqoaHA4roPRzRfAr9/5KXEhRJJaUMPpBwBo0BvOxd/pNmZZkx91OUGA1Cvlo0vGNLfxHHyzrs4QJBAK/E7xCTdZv9VQfnf8t4S+sGNB2UkUtQyGyf1yXfAfF4tm4j6Q7rO18RuI2XO6tRg5z5k4HVoaf8gqtenCdyo8o=");
//            genKeyPair();
        } catch (Exception e) {
            log.error("生成key异常：", e);
        }
    }

    /**
     * 随机生成密钥对
     */
    public static void genKeyPair() throws NoSuchAlgorithmException {
        // KeyPairGenerator类用于生成公钥和私钥对，基于RSA算法生成对象
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        // 初始化密钥对生成器
        keyPairGen.initialize(KEY_SIZE, new SecureRandom());
        // 生成一个密钥对，保存在keyPair中
        KeyPair keyPair = keyPairGen.generateKeyPair();
        // 得到私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        // 得到公钥
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        String publicKeyString = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        // 得到私钥字符串
        String privateKeyString = Base64.getEncoder().encodeToString(privateKey.getEncoded());
        // 将公钥和私钥保存到Map
        //0表示公钥
        keyMap.put(PUBLIC_KEY, publicKeyString);
        //1表示私钥
        keyMap.put(PRIVATE_KEY, privateKeyString);
    }

    /**
     * RSA公钥加密
     *
     * @param str       加密字符串
     * @param publicKey 公钥
     * @return 密文
     * @throws Exception 加密过程中的异常信息
     */
    public static String encrypt(String str, String publicKey) throws Exception {
        //base64编码的公钥
        byte[] decoded = Base64.getDecoder().decode(publicKey);
        RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
        //RSA加密
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        String outStr = Base64.getEncoder().encodeToString(cipher.doFinal(str.getBytes("UTF-8")));
        return outStr;
    }

    /**
     * RSA私钥解密
     *
     * @param str        加密字符串
     * @param privateKey 私钥
     * @return 明文
     * @throws Exception 解密过程中的异常信息
     */
    public static String decrypt(String str, String privateKey) throws Exception {
        //64位解码加密后的字符串
        byte[] inputByte = Base64.getDecoder().decode(str);
        //base64编码的私钥
        byte[] decoded = Base64.getDecoder().decode(privateKey);
        RSAPrivateKey priKey = (RSAPrivateKey) KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(decoded));
        //RSA解密
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, priKey);
        String outStr = new String(cipher.doFinal(inputByte));
        return outStr;
    }

    public static void main(String[] args) throws Exception {
        long temp = System.currentTimeMillis();
        //生成公钥和私钥
        genKeyPair();
        //加密字符串
        System.out.println("公钥:" + keyMap.get(PUBLIC_KEY));
        System.out.println("私钥:" + keyMap.get(PRIVATE_KEY));
        System.out.println("生成密钥消耗时间:" + (System.currentTimeMillis() - temp) / 1000.0 + "秒");

        String message = "ddssssssssssssss";
        System.out.println("原文:" + message);
        temp = System.currentTimeMillis();
        String messageEn = encrypt(message, keyMap.get(PRIVATE_KEY));

        System.out.println("密文:" + messageEn);
        System.out.println("加密消耗时间:" + (System.currentTimeMillis() - temp) / 1000.0 + "秒");

//        temp = System.currentTimeMillis();
//        String messageDe = decrypt(messageEn, keyMap.get(PUBLIC_KEY));
//        System.out.println("解密:" + messageDe);
//        System.out.println("解密消耗时间:" + (System.currentTimeMillis() - temp) / 1000.0 + "秒");
    }

}
