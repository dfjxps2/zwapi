package com.dfjinxin.fw.service.commons.id;

import com.dfjinxin.fw.service.commons.id.impl.SnowflakeIdGenerator;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ConfigurationProperties(prefix = "dfjinxin.core.idgenerator")
@Setter
@Slf4j
public class IdGeneratorConfiguration {
    private Long datacenter = 1L;
    private Long worker = 1L;

    @Bean("idGenerator")
//    @ConditionalOnProperty(name = "dfjinxin.core.idgenerator.mode", havingValue = "snowflake")
    public IdGenerator snowflakeIdGenerator() {
        return new SnowflakeIdGenerator(datacenter, worker);
    }
}
