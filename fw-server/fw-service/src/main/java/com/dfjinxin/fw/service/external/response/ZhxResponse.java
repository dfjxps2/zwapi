package com.dfjinxin.fw.service.external.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ZhxResponse implements Serializable {

    private String code;//返回代码，成功00，异常44，参数有误11，

    private String msg;//成功、失败信息提示

    private Object data;

    public static ZhxResponse ok(){
        return new ZhxResponse().setCode("00").setMsg("请求成功。");
    }

    public static ZhxResponse excep(){
        return new ZhxResponse().setCode("44").setMsg("请求异常。");
    }

    public static ZhxResponse invalidExcep(){
        return new ZhxResponse().setCode("11").setMsg("请求参数有误，请确认。");
    }
}
