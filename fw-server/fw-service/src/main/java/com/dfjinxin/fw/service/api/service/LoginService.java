package com.dfjinxin.fw.service.api.service;

import com.dfjinxin.fw.service.api.constant.FwConstant;
import com.dfjinxin.fw.service.api.constant.RSAUtils;
import com.dfjinxin.fw.service.modules.entity.RequestToken;
import com.dfjinxin.fw.service.modules.service.RequestTokenService;
import com.travelsky.dib.odats.profpsr.ms.bas.config.NativaMscBuild;
import com.travelsky.dss.sc.nativeclient.msc.context.TokenStorageContext;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthException;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthTokenOfflineException;
import com.travelsky.dss.sc.nativeclient.msc.exception.RequestBusinessRuntimeException;
import com.travelsky.dss.sc.nativeclient.msc.exception.dto.BusinessExceptionInfoDTO;
import com.travelsky.dss.sc.nativeclient.msc.web.context.AbstractTokenStorage;
import com.travelsky.dss.sc.nativeclient.msc.web.context.dto.TokenContentDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class LoginService {

    @Value("${api-server-url}")
    private String authUrl;

    @Autowired
    private RequestTokenService requestTokenService;

    public void initialize(){
        List<String> serverAddr = new ArrayList<>();
        serverAddr.add(authUrl);//服务器地址待确定后提供
        NativaMscBuild.build(FwConstant.systemId, FwConstant.username, RSAUtils.keyMap.get(RSAUtils.PRIVATE_KEY), serverAddr);
    }

    public void createToken(){
        List<String> serverAddr = new ArrayList<>();
        //自定义类 XXXX 实现抽象类 AbstractTokenStorage
        // 重写里面的方法，可以用 redis 或者数据库来进行存放 token
        serverAddr.add(authUrl);//#服务器地址待确定后提供
        try {
            NativaMscBuild.buildOwnToken(FwConstant.systemId, FwConstant.username, RSAUtils.keyMap.get(RSAUtils.PRIVATE_KEY), serverAddr, new AbstractTokenStorage(){
                @Override
                public void putToken(String systemId, TokenContentDTO token) {
                    log.info("接收token：{}", systemId);
                    log.info("接收token：{}", token.getToken());
                    log.info("接收token：{}", token);
                    RequestToken requestToken = new RequestToken();
                    requestToken.setSystemId(systemId);
                    requestToken.setToken(token.getToken());
                    requestToken.setStatus(token.getOfflineStatus());
                    requestTokenService.save(requestToken);
                }

                @Override
                public TokenContentDTO getToken(String systemId) {
                    log.info("查询token:{}", systemId);
                    TokenContentDTO tokenContentDTO = new TokenContentDTO();
                    RequestToken requestToken = requestTokenService.queryToken(systemId);
                    if(null == requestToken){
                        return null;
                    }
                    tokenContentDTO.setToken(requestToken.getToken());
                    log.info("返回token:{}", tokenContentDTO);
                    return tokenContentDTO;
                }

                @Override
                public boolean lock(String systemId){
                    return true;
                }

                @Override
                public void unLock(String systemId){

                }

                @Override
                public void removeToken(String systemId) {

                }
            });
        }catch (Exception e){
            log.error("初始化异常信息：{}", e);
        }

    }
}
