package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearBookingInfo;

/**
 * 历史半年进出岛旅客订票信息
 */
public interface TsgptZhxHalfyearBookingInfoService extends IService<TsgptZhxHalfyearBookingInfo> {

    boolean create(TsgptZhxHalfyearBookingInfo info);
}
