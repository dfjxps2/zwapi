package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHighFrequencyInfo;

/**
 * 旅客低价高频信息
 */
public interface TsgptZhxHighFrequencyInfoService extends IService<TsgptZhxHighFrequencyInfo> {

    boolean create(TsgptZhxHighFrequencyInfo info);
}
