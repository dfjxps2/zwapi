package com.dfjinxin.fw.service.mpp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxThreeBookingInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 今明后三天进出岛旅客订票信息
 */
@Repository
@Mapper
public interface TsgptZhxThreeBookingInfoMpp extends BaseMapper<TsgptZhxThreeBookingInfo> {

}
