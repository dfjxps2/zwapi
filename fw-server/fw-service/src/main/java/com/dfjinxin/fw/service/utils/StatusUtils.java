package com.dfjinxin.fw.service.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class StatusUtils {

    /**
     * 启用状态
     */
    public String STATUS_ENABLED = "1";

    /**
     * 禁用状态
     */
    public String STATUS_DISABLED = "0";

    /**
     * 禁用状态
     */
    public Integer STATUS_DISABLED_INT = 0;

    /**
     * 标签分类启用状态
     */
    public Integer STATUS_ENABLED_CATEGORY = 2;




}
