package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxAnomalousInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxAnomalousInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxAnomalousInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 旅客安检信息
 */
@Service
public class TsgptZhxAnomalousInfoServiceImpl extends ServiceImpl<TsgptZhxAnomalousInfoMpp, TsgptZhxAnomalousInfo> implements TsgptZhxAnomalousInfoService {

    @Autowired
    private IdGenerator idGenerator;

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxAnomalousInfo info) {
        return this.save(info);
    }

    @DataSource("mpp")
    @Override
    public boolean createByDto(PassengerDTO dto) {
        /**
         * 旅客安检信息
         */
        TsgptZhxAnomalousInfo tsgptZhxAnomalousInfo = new TsgptZhxAnomalousInfo(dto);
        tsgptZhxAnomalousInfo.setId("" + idGenerator.getId());
        return this.save(tsgptZhxAnomalousInfo);
    }
}
