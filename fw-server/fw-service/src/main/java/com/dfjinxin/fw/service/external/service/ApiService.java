package com.dfjinxin.fw.service.external.service;

import com.dfjinxin.fw.service.mpp.entity.*;

import java.util.List;

public interface ApiService {


    List<TsgptZhxAnomalousInfo> zhxApi31AllOrOneDay(String day);

    List<TsgptZhxHighFrequencyInfo> zhxApi312(int travelCount, int priceDiscount);

    List<TsgptZhxThreeBookingInfo> zhxApi313(String param);

    List<TsgptZhxHalfyearBookingInfo> zhxApi314(String param);

    List<TsgptZhxTodayOutislandsInfo> zhxApi315(String param);

    List<TsgptZhxHalfyearOutislandsInfo> zhxApi316(String param);

    List<TsgptZhxThreeColleaguesInfo> zhxApi317(String param);

    List<TsgptZhxHalfyearColleaguesInfo> zhxApi318(String param);
}
