package com.dfjinxin.fw.service.mpp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearBookingInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TsgptZhxHalfyearBookingInfoMpp extends BaseMapper<TsgptZhxHalfyearBookingInfo> {

}
