package com.dfjinxin.fw.service.api.service;

import com.dfjinxin.fw.service.api.constant.FwConstant;
import com.travelsky.dss.sc.nativeclient.msc.context.TokenStorageContext;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthException;
import com.travelsky.dss.sc.nativeclient.msc.exception.AuthTokenOfflineException;
import com.travelsky.dss.sc.nativeclient.msc.exception.RequestBusinessRuntimeException;
import com.travelsky.dss.sc.nativeclient.msc.exception.dto.BusinessExceptionInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FwApiService {

    public void throwableExeception(String className, Throwable e){
        log.error("【"+ className +"】" + "调用接口异常: {}", e);
        if (e instanceof AuthException) {
            TokenStorageContext.getStorage().cleanOfflineStatus(FwConstant.systemId);

        }
        if (e instanceof AuthTokenOfflineException) {
            //TokenStorageContext.getStorage().cleanOfflineStatus(systemId);
        }
        if (e instanceof RequestBusinessRuntimeException) {
            BusinessExceptionInfoDTO infoDTO = ((RequestBusinessRuntimeException) e).exceptionInfo();
            // 后续处理
            log.error("code后续处理: {}", infoDTO.getCode());
            log.error("Message后续处理: {}", infoDTO.getMessage());
        }
    }

}
