package com.dfjinxin.fw.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FwServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FwServiceApplication.class, args);
    }

}
