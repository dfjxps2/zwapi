package com.dfjinxin.fw.service.utils;

import com.google.gson.Gson;

public class SysJsonUtils {
    private static Gson gson = new Gson();

    public static String objectsToJson(Object[] os){
        try {
            return gson.toJson(os);
        }catch (Exception e){

        }
        return "";
    }

    public static String objectToJson(Object o){
        try {
            return gson.toJson(o);
        }catch (Exception e){

        }
        return "";
    }

}
