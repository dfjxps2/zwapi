package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxHalfyearColleaguesInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearColleaguesInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHalfyearColleaguesInfoService;
import org.springframework.stereotype.Service;

/**
 * 历史半年旅客进出岛同行人信息
 */
@Service
public class TsgptZhxHalfyearColleaguesInfoServiceImpl extends ServiceImpl<TsgptZhxHalfyearColleaguesInfoMpp, TsgptZhxHalfyearColleaguesInfo> implements TsgptZhxHalfyearColleaguesInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxHalfyearColleaguesInfo info) {
        return this.save(info);
    }

}
