package com.dfjinxin.fw.service.api.service.busi;

import com.alibaba.fastjson.JSONObject;
import com.dfjinxin.fw.service.api.service.FwApiService;
import com.dfjinxin.fw.service.api.service.RunService;
import com.dfjinxin.fw.service.commons.id.IdGenerator;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearBookingInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHalfyearBookingInfoService;
import com.travelsky.dib.odats.profpsr.ms.bas.resource.SmpPsrResource;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.EtTravelDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 获取历史半年民航旅客进出岛订票信息接口
 */
@Slf4j
@Service
public class Api314Service implements RunService {

    @Autowired
    private TsgptZhxHalfyearBookingInfoService tsgptZhxHalfyearBookingInfoService;
    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private FwApiService fwApiService;

    @Override
    public void run(String param) {

        /**
         * pasName String 张三 旅客姓名
         * certType String NI 旅客证件类型
         * certNo String 12345678 旅客证件号
         * fltNumber String HU7281 航班号
         * fltDate String 2020-07-02 航班日期
         * airln String 海南航空 航空公司
         * dptAirport String 海口美兰 起始机场
         * dptCity String 海口 起始城市
         * arrAirport String 北京首都 到达机场
         * arrCity String 北京 到达城市
         * pasTkne String 880122344556 旅客电子客票号
         * issueTime String 2020-05-01 12:20 出票时间
         * priceDiscount Double 1.25 票价折扣
         */
        try {
            List<EtTravelDTO> etTravelCkiDTOList = SmpPsrResource.queryEtTravelHisByCert(param);
            if(null != etTravelCkiDTOList && etTravelCkiDTOList.size() > 0){
                log.info("旅客证件号：{}, 接收总数：{}", param, etTravelCkiDTOList.size());
                for(EtTravelDTO dto : etTravelCkiDTOList){

                    /**
                     * 历史半年进出岛旅客订票信息
                     */
                    TsgptZhxHalfyearBookingInfo info = new TsgptZhxHalfyearBookingInfo(dto);
                    info.setId("" + idGenerator.getId());
                    tsgptZhxHalfyearBookingInfoService.create(info);
                }
            }
        }catch (Throwable e) {
            fwApiService.throwableExeception("获取历史半年民航旅客进出岛订票信息接口", e);
        }

    }
}
