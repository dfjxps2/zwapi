/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.dfjinxin.fw.service.modules.task;

import com.dfjinxin.fw.service.api.service.busi.Api31All180Service;
import com.dfjinxin.fw.service.api.service.busi.Api31OneDayService;
import com.dfjinxin.fw.service.commons.core.exception.ServiceException;
import com.dfjinxin.fw.service.commons.utils.CodeUtils;
import com.dfjinxin.fw.service.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 获取民航离岛旅客安检异常信息接口
 */
@Slf4j
@Component("api31Task")
public class Api31Task implements ITask {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private Api31All180Service api31All180Service;
	@Autowired
	private Api31OneDayService api31OneDayService;

	@Override
	public void run(String params){
		logger.info("开始执行，参数为：{}", params);

		if(StringUtils.isBlank(params)){
			throw new ServiceException(CodeUtils.EXCEPTION_CODE, "【获取民航离岛旅客安检异常信息接口】任务执行设置参数不能为空");
		}

		/**
		 *  查询全部180天数据
		 */
		if("all".equals(params)){
			api31All180Service.run(null);
		}

		if("everyday".equals(params)){
			/**
			 * 查询前一天的数据，例如：2020-07-09
			 */
			params = DateUtils.PreviousDay();
			api31OneDayService.run(params);

		} else {
			/**
			 * 直接传参查询哪一天数据，例如：2020-07-09
			 */
			api31OneDayService.run(params);
		}

		logger.info("执行结束，参数为：{}", params);

	}
}
