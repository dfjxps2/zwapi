package com.dfjinxin.fw.service.mpp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearOutislandsInfo;

/**
 * 历史半年离岛旅客值机安检信息
 */
public interface TsgptZhxHalfyearOutislandsInfoService extends IService<TsgptZhxHalfyearOutislandsInfo> {

    boolean create(TsgptZhxHalfyearOutislandsInfo info);
}
