package com.dfjinxin.fw.service.mpp.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.PassengerDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 旅客低价高频信息
 */
@Data
@Accessors(chain = true)
@TableName("t_sgpt_zhx_high_frequency_info")
public class TsgptZhxHighFrequencyInfo implements Serializable {

    public TsgptZhxHighFrequencyInfo(){
        super();
    }

    public TsgptZhxHighFrequencyInfo(PassengerDTO dto){
        super();
        this.pasname = dto.getPasName();

        /**
         *旅客证件类型
         */
        this.certtype = dto.getCertType();

        /**
         *旅客证件号码
         */
        this.certno = dto.getCertNo();
    }

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     *旅客姓名
     */
    private String pasname;

    /**
     *旅客证件类型
     */
    private String certtype;

    /**
     *旅客证件号码
     */
    private String certno;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createtm;

}
