package com.dfjinxin.fw.service.mpp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dfjinxin.fw.service.datasource.annotation.DataSource;
import com.dfjinxin.fw.service.mpp.dao.TsgptZhxHalfyearBookingInfoMpp;
import com.dfjinxin.fw.service.mpp.entity.TsgptZhxHalfyearBookingInfo;
import com.dfjinxin.fw.service.mpp.service.TsgptZhxHalfyearBookingInfoService;
import org.springframework.stereotype.Service;

/**
 * 历史半年进出岛旅客订票信息
 */
@Service
public class TsgptZhxHalfyearBookingInfoServiceImpl extends ServiceImpl<TsgptZhxHalfyearBookingInfoMpp, TsgptZhxHalfyearBookingInfo> implements TsgptZhxHalfyearBookingInfoService {

    @DataSource("mpp")
    @Override
    public boolean create(TsgptZhxHalfyearBookingInfo info) {
        return this.save(info);
    }

}
