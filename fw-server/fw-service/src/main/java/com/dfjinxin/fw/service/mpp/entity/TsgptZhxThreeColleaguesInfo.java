package com.dfjinxin.fw.service.mpp.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.travelsky.dib.odats.profpsr.ms.bas.smp.model.CompanionDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 今明后旅客进出岛同行人信息
 */
@Data
@Accessors(chain = true)
@TableName("t_sgpt_zhx_three_colleagues_info")
public class TsgptZhxThreeColleaguesInfo implements Serializable {

    public TsgptZhxThreeColleaguesInfo(){
        super();
    }

    public TsgptZhxThreeColleaguesInfo(String param, CompanionDTO dto){
        super();
        /**
         *'旅客证件号码'
         */
        this.querycertno = param;

        /**
         *'同行人旅客姓名'
         */
        this.pasname = dto.getPasName();

        /**
         *'同行人旅客证件类型'
         */
        this.certtype = dto.getCertType();

        /**
         *'同行人旅客证件号码'
         */
        this.certno = dto.getCertNo();

        /**
         *'航班号'
         */
//                this.Fltnumber();

        /**
         *'航班日期'
         */
        this.fltdate = dto.getFltDate();

        /**
         *'航空公司'
         */
        this.airln = dto.getAirln();

        /**
         *'起始机场'
         */
        this.dptairport = dto.getDptAirport();

        /**
         *'起始城市'
         */
        this.dptcity = dto.getDptCity();

        /**
         *'到达机场'
         */
        this.arrairport = dto.getArrAirport();

        /**
         *'到达城市'
         */
        this.arrcity = dto.getArrCity();
    }

    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     *'旅客证件号码'
     */
    private String querycertno;

    /**
     *'同行人旅客姓名'
     */
    private String pasname;

    /**
     *'同行人旅客证件类型'
     */
    private String certtype;

    /**
     *'同行人旅客证件号码'
     */
    private String certno;

    /**
     *'航班号'
     */
    private String fltnumber;

    /**
     *'航班日期'
     */
    private String fltdate;

    /**
     *'航空公司'
     */
    private String airln;

    /**
     *'起始机场'
     */
    private String dptairport;

    /**
     *'起始城市'
     */
    private String dptcity;

    /**
     *'到达机场'
     */
    private String arrairport;

    /**
     *'到达城市'
     */
    private String arrcity;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createtm;
}
